import cffi
from glob import glob
import os.path as op

path = op.relpath(op.dirname(__file__))

cfiles = [x for x in glob("%s/C/*.c" % path) if not x.endswith("main.c")]
hfiles = [h for h in glob('%s/C/*.h' % path)]

ffibuilder = cffi.FFI()
ffibuilder.set_source("tabix._tabixffi",'''
#include "stdlib.h"
#include "tabix.h"
''',
    libraries=['c', 'z'],
    depends=hfiles + cfiles,
    sources=cfiles,
    include_dirs=["%s/C" % path],
)

ffibuilder.cdef("""

struct __ti_index_t;
typedef struct __ti_index_t ti_index_t;

struct __ti_iter_t;
typedef struct __ti_iter_t *ti_iter_t;

typedef struct {
    ti_index_t *idx;
    ...;
} tabix_t;

typedef struct {
        int32_t preset;
        int32_t sc, bc, ec; // seq col., beg col. and end col.
        int32_t meta_char, line_skip;
} ti_conf_t;

tabix_t *ti_open(const char *fn, const char *fnidx);
int ti_lazy_index_load(tabix_t *t);
int ti_index_build(const char *fn, const ti_conf_t *conf);
void ti_close(tabix_t *t);
void free (void* ptr);

const char **ti_seqname(const ti_index_t *idx, int *n);

ti_iter_t ti_query(tabix_t *t, const char *name, int beg, int end);
ti_iter_t ti_querys(tabix_t *t, const char *reg);

const char *ti_read(tabix_t *t, ti_iter_t iter, int *len);
void ti_iter_destroy(ti_iter_t iter);

const ti_conf_t *ti_get_conf(ti_index_t *idx);

void ti_index_destroy(ti_index_t *idx);
""")

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)


